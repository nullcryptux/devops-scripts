#!/usr/bin/env bash

set -e 

CONTEXT="$1"

if [[ -z ${CONTEXT} ]]; then
  echo "Usage: $0 kube-context"
  exit 1
fi

NAMESPACES=$(kubectl --context ${CONTEXT} get -o json namespaces | jq '.items[].metadata.name' -r)
RESOURCES="pod deployment configmap ingress secret service"

for ns in ${NAMESPACES}; do
  for resource in ${RESOURCES}; do
    echo
    echo "Extracting ${RESOURCES}"
    rsrcs=$(kubectl --context ${CONTEXT} -n ${ns} get -o json ${resource} | jq '.items[].metadata.name' -r)
    for r in ${rsrcs}; do
      dir="${CONTEXT}/${ns}/${resource}"
      echo
      echo "Creating directory ${dir}"
      mkdir -p "${dir}"
      kubectl --context ${CONTEXT} -n ${ns} get -o yaml ${resource} ${r} > "${dir}/${r}.yaml"
      echo
      echo "YAML saved to ${dir}/${r}.yaml"
    done
  done
done